
<!DOCTYPE html>
<html>
<body>

<h1>Welcome to</h1>
<img src="MetIgel_logo.jpg" title="Title" width = 600 height = 400> <br><br>

Authors:
Linnea C. Smith and Andreas Y. Schedl, Molecular Interaction Ecology, iDiv Halle-Jena-Leipzig, 2021 <br>

Logo created by Jennifer Gabriel, Molecular Interaction Ecology, iDiv Halle-Jena-Leipzig, 2021 <br><br>

ISC License<br><br>

Copyright (c) 2021, Linnea C. Smith and Andreas Y. Schedl<br><br>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.<br><br>

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.<br>
<br>

<h3>Disclaimer</h3>

This has been an exciting side project for us, but we don't have time to provide full-time support. You may notify us of issues (always provide a full error log!), but we cannot promise we will get to it quickly.<br>
For bug reporting, feature requests, other ideas and contribution please refer to our support channel: <br>
https://talk.idiv.de/idiv/channels/metigel


<br>
We are planning a second release in the indefinite future, so some features may change or be updated then.
<br>

<h2>Introduction</h2>
Untargeted metabolomic analysis requires considerable post-processing and tedious reformatting in order to enable multivariate statistical analysis. MetIgel is a post-processing tool which allows the user to visualize and analyze feature tables from untargeted metabolomics experiments, with a primary focus on ecometabolomics and the techniques often applied in that field. As the chemical class of a compound, rather than the compound’s identity in and of itself, is often relevant in ecometabolomic analyses, MetIgel also offers interfaces to classification via two chemical ontologies. All analyses can be carried out at any level of either of these ontologies, enabling the user to perform the tests most relevant to their question.

<br><br>

<h2>File formatting</h2>

Many preprocessing softwares can be used to get from raw mass spectrometry data to a feature table as required by MetIgel. A couple examples are MetaboScape, SIRIUS/CANOPUS, MSDial, and many more.
Each of these softwares gives a slightly differently formatted feature table. Before using the rest of MetIgel, both your feature table and the treatment information for your samples should be uploaded to the formatting wizard to ensure it is in the correct format for further processing.
<br><br>
Upload the file you wish to have formatted and choose the step you want to use that file for under "What type of file would you like to upload?". The options giving each correspond to one of the workflow steps as listed in the sidebar. Some steps require two files; make sure to choose both the appropriate step and the appropriate file description. 
Dropdown lists appear with columns required by MetIgel for the chosen step in bold, with dropdown lists containing your column names. Choose the column of your dataset that best corresponds to each required column. 
<br><br>
Example: I have a feature table with annotations, but no InChIKeys, saved as a CSV. I did not use MetaboScape for my data preprocessing, but I do have the library MSP files I used for the annotation.
I upload my feature table under the “CSV Files” section and select “Feature table without InChIKey/SMILES” as the type of file. To the right, drop down menus appear with the titles, “FeatureID”, "RT (Retention time)", "m/z", "Name of compound", and "Formula". "Sample columns (intensities)" should be the columns containing the feature intensities for each sample. All the columns in the feature table are automatically selected. 
I remove all the columns that do not contain feature intensities, hit "Reformat file", and then save the new file to my preferred location with a name that makes clear what exactly it is.
<br><br>
I then go to the “MSP library to CSV” tab, select my MSP files to convert them from MSP to CSV, and progress to the “Combine library and features” tab, where I use my two newly-obtained CSV files.
This merges the InChIKey information contained in the MSP with my annotations, leaving me with a feature table including the InChIKey for each of its annotations.

<br><br>

<h3>CSV Files</h3>
For each of the CSV reformatting options (except for MetaboScape), you are presented with a list of the categories/columns the chosen format requires. Each column has a dropdown list with the column names of the file you've uploaded. Choose the column from your file that most closely fits that listed. If there is no match for one of the categories (e.g., if your preprocessing software doesn't include m/z, for whatever reason), then choose NA for that category.
<ul>
  <li><i>Feature table without InChIKey/SMILES</i></li>
  A feature table including the intensity of each feature in each sample, but not including the InChIKey or SMILES. Samples in columns, features in rows. There must also at minimum be a column for feature name, to allow matching with MSP library entries to then extract the InChIKey and SMILES for classification.
  <br><br>
  <li><i>Feature table from Metaboscape pre-v5.0</i></li>
  MetaboScape v4.0 and prior exports an annotated feature table with a specific format -- the treatment groups of each sample are in the first rows, and the InChIKey and SMILES are not exported along with the name of the annotation. See the image below for an example. If you have a feature table from MetaboScape v4.0 or prior, you can upload this feature table directly. This will give you both a reformatted feature table and a MetIgel-formatted table of your treatment data, both of which you need moving forward.
  <br><br>
  <img src="metaboscape_output_example.png" title="Example of MetaboScape output" width = 800 height = 450> <br><br>
  <li><i>Chemical identifiers for annotations</i></li>
  If you have a CSV file containing the InChIKey and/or SMILES that correspond to the molecule names used in your annotations, you can upload this here. (E.g., similar to the output of MSP library to CSV -- see below).
  <br><br>
  <li><i>Feature table with InChIKey/SMILES, without classification</i></li>
  A feature table including the annotations and the InChIKey and/or SMILES of each annotation, which you wish to run through ClassyFire and/or Natural Products classification.
  <br><br>
  <li><i>Classified, annotated feature table</i></li>
  A feature table including the ClassyFire and/or Natural Products classification of each annotation, which you wish to use for visualization and statistics.
  <br><br>
  <li><i>Sample metadata/Treatment groups</i></li>
  A table containing the names of your samples, as denoted in your feature table, and the treatment group(s) for each. You can have as many treatment groups as you wish. Please note you may NOT name your treatment group "group". If you don't have a more descriptive name for your treatment, you may use "group1" or any modification on the word "group", but only using "group" will lead to errors in the sunburst plot and statistics sections.
</ul>

<h3>MSP library from CSV</h3>

This step takes spectral libraries in MSP file format, extracts the name, SMILES, and InChIKey of each compound in each of those libraries, and assembles this metadata into a single dataframe. You can simultaneously upload multiple MSP files for combination.<br> <br>


<img src="msp_upload.png" title="Library loading" width = 800 height = 450> <br><br>

<i>Splitting large .msp files into 200mb parts is recommended for processing</i> <br><br>

The user has the chance to save the processed libraries to .csv or .rds file format for later use. Saving is STRONGLY recommended, as this step can be extremely time-consuming and so it's better to do it once and save the output, so you can skip this step going forward!<br>

<i>Processing duration depends on how many libraries you load, and on your system specifications.</i> <br><br>


<h2>Combine library & features</h2>
If your annotated feature table does not include InChIKeys and/or SMILES, and you have a separate table of these chemical identifiers for the molecule names used in your annotations (i.e., MSP library converted to CSV above), use this step to add the chemical identifiers to your feature table. The feature table will be joined with the library data based on the compound name, so the exact spelling used in your feature table must also be present in the library file.
In this part the user loads the annotated MetaboScape output CSV file. The user has the chance to either use the library files they read in in the previous step, or to load previously saved ones. Remember to click the load button - just selecting the file is not enough! <br><br>

This step also checks your combined libraries for disagreements (e.g. if you've uploaded two separate libraries, and both include a compound with the same name, but different Inchikeys, or vice versa). It is up to you what you choose to do with this information -- disagreements most often arise from varying spellings of the same compound and are often safe to ignore. However, if it concerns you, you can download the file showing the disagreements and manually check them. <br><br>


In case one compound is listed in more than one of your libraries, and/or one compound has more than one SMILES in a given library, the very first entry in the library dataframe will be taken. <br> <br>

The final result from this step is the joined MetaboScape output with the InChIKeys and Smiles.<br>

<h3>SIRIUS/CANOPUS</h3>

If you annotated your data using SIRIUS and then used CANOPUS for classification, you can load the CANOPUS classifications into MetIgel for visualization and statistical analysis. The CANOPUS output should include a file called canopus_compound_summary.tsv. This contains the classifications for each feature you loaded into SIRIUS. You should upload this exactly as-is in the appropriate place. 

<br><br>

When you began SIRIUS, you also had some feature table from whatever software you used to preprocess your files. This feature table should include the feature names, as uploaded into SIRIUS, and the intensities of each feature in each sample. You should first format this feature table using the CSV Files/Feature table without InChIKey/SMILES option (described above), and then upload that reformatted file here. Then click "Create feature table from SIRIUS output" to combine the CANOPUS classifications with your feature table of intensities. Save this and use it for visualization and statistical analysis. (Note that you will also need your table of sample treatment groups as described above.)

<br><br>
<h4>Coming from MZMine3</h4>
If you used MZMine3 to preprocess your data for SIRIUS, you should save your feature table in the offered MetaboAnalyst format. This feature table will require one additional step before you can load it into the file formatter as desribed in the previous paragraph. In your MetIgel project folder, go to the folder called R and open the file "mzmine_to_metigel.R". Type or copy-paste the path to your saved MZMine3 feature table CSV at the top of the script, where it says to do so. You can then highlight the entire script and run it. This will save a reformatted feature table to the same folder you read your feature table in from, with "for_MetIgel" added onto the end of the file name. Use this new feature table in the file reformatting wizard and SIRIUS tab as described above.

<br><br>
Explanation: MZMine adds an additional row to its feature table including either a treatment group or the run date, and names its features according not only to an ID number but also with the m/z and retention times. This extra row needs to be removed from the feature table. So that the feature names match up with those in the .mgf file MZMine3 produces for SIRIUS, the m/z values and retention times must be removed from the feature IDs. These are the two steps performed in this small additional script.

<h2>Get classifications</h2>

<h3>ClassyFire</h3>
The annotated data is handed over to the Classyfire API. If any entries do not include SMILES, an attempt will first be made to look up the SMILES using PubChem. Compounds are then classified according to the ClassyFire ontology.<br>

<b>Depending on how many annotations you have, THIS STEP can take a loooooooong time.</b> You'll know it's done when the "Preview" window is populated with a large dataframe that you can only see a part of. We again recommend that you save this dataframe at this point!<br>

<h3>Natural Products</h3>
Classify your annotations according to the Natural Products ontology using the chemodiv package (Petren et al. 2022). If you have just performed the ClassyFire classification, leave "Use ClassyFire Output" checked and click "Use file from previous step". If for any reason you don't want the ClassyFire classification, uncheck this box. NOTE: the output table will still include columns for ClassyFire classifications, but those columns will be empty. However, their presence is required for further use in MetIgel.

<br><br>

Like ClassyFire, this step can take a long time. Natural Products classification can return multiple possible classifications for a single annotation. If this is the case, extra columns will be used to denote alternate classifications. However, currently, all further analysis is done using the first classification.

<h2>Sunburst</h2>

<h3>Group your data</h3>

You can either use the output of the Natural Products classification step directly using the "Use file from previous step" button, or upload a classified, annotated feature table from your files. This is can be the output of one or both of the classification steps, the output of the SIRIUS/CANOPUS file formatter, or the output of the CSV reformatting option "Classified, annotated feature table".

<br><br>

The sample groupings file is the sample treatment information file as generated in the CSV reformatting option "Sample metadata/Treatment groups", or the sample grouping output of the "Feature table from MetaboScape v.4.0".

<br><br>

In order to create sunburst plots, the data must first be aggregated according to treatment groups which are chosen by the user. First, the data is aggregated based on user-defined sample groupings (e.g., could be control and treatment; or root, stem, and leaf extracts). If you have multiple treatments, you can aggregate samples based on as few or as many as you like; each combination of treatments will be regarded as a discrete group (e.g., control-root, control-stem control-leaf, treatment-root, treatment-stem, treatment-leaf). Second, this grouped data is further aggregated by either ClassyFire or Natural Products chemical compound class.

<br><br>

You can save the output of each grouping step separately. At the end of the second grouping, you have a dataset showing the total (summed) intensity of each chemical class in each of your sample groups. In addition to summed intensities, MetIgel also generates datasets of the richness of each chemical class, i.e., the count data of how many features are in each class.<br>

<img src="data_grouping.png" title = "Grouping data" width = 700 height =350> <br><br>

<br><br>

<h3>Sunburst plots</h3>
There are several options for making sunburst plots. You can choose to include or exclude unclassified compounds, which are very common in ecometabolomics data:
<img src="sunburst_with_unclassified.png" title = "Sunburst plots of data including unclassified compounds." width = 800 height = 350> <br>
<img src="sunburst_without_unclassified.png" title = "Sunburst plots of the same data, with unclassified compounds removed." width = 800 height = 350> <br><br>

You may also choose which chemical classes you wish to represent in your sunburst plots. You must include at least two levels.

<br><br>

You can generate both static sunburst plots, which can be used in reports and publications, and interactive plots which can be used in presentations or for online distribution.

<br><br>

For each treatment group, both intensity-based and richness-based plots are generated. The intensity-based plot shows the summed intensity of each class plotted. The richness-based plot scales the representation of each class in the sunburst plot based on how many different under-classes are in each parent class, and how many samples they occur in. I.e., imagine your data includes the ClassyFire subclasses "Amino acids, peptides, and analogues" and "Carboxylic acid derivatives". These classes occur in five of your ten samples. Both of these belong to the ClassyFire class "Carboxylic acids and derivatives"; therefore, this class has a richness of 2 * 5, so a richness of 10.

This may be changed in the future, so that the richness shows solely how many features are present from each class, without accoutning for how many samples those features occur in.

<br> <br>

The user can choose which file format and DPI is used for saving static plots (.png or .svg). <br>
The interactive plots are saved as .html files to be viewed in a browser or embedded in different presentation softwares. You can also choose to save all generated plots at once as a .zip file.

<br> <br>

<h2>Statistics</h2>

<h3>Upload data</h3>

Upload your annotated, classified feature table. This is can be the output of one or both of the classification steps, the output of the SIRIUS/CANOPUS file formatter, or the output of the CSV reformatting option "Classified, annotated feature table".

<br><br>

The sample groupings file is the sample treatment information file as generated in the CSV reformatting option "Sample metadata/Treatment groups", or the sample grouping output of the "Feature table from MetaboScape v.4.0".

<br><br>

<h3>Data transformation</h3>

Choose which level of chemical classification you want to perform your analysis with. 
I.e., do you want to look at differences in chemical subclasses between your treatments, or one level higher at the class level? Then, choose the normalization, transformation, and/or standardization you wish to apply and click "Apply chosen operations".
Make sure to read about these statistical transformations and choose one that are most suitable for your data and the analysis you want to perform.

<br><br>

The distribution of your data is shown in the bar plots on the right-hand side of the screen. The "samples" bar plot shows the distribution of intensities across your chosen chemical classification level.
As above, the intensities are summed so that there is one row per class (see image). The "features" bar plot shows the distribution of samples across each chemical class in your chosen classification level.
After you perform your transformation, additional bar plots will appear showing the effect your transformations had on the distributions.
<br><br>

You may also choose to display raincloud plots instead of simple barplots. This is recommended for visualizing a small number of features or samples (5-15).<br>

<br><br>
Once you are satisfied with your transformations, save your transformed data and proceed to your desired statistical tests.
<br><br>

<h3>PCoA</h3>
Perform principle coordinates analysis. Follow the links on the page for background information on PCoA. You must press the Calculate PCoA button each time you want to calculate a new one, i.e. if you want to try a different dissimilarity matrix, you choose the one you want and then press the button.
You can also plot the first two PCoA axes and choose by which treatment you would like to color the points. Each time you choose another color scheme, you must again press the plot button.

<br><br>

Downloading the numerical results gives you an Excel file. The Notes tab shows the correction applied, any notes given by the PCoA function, and the trace. The Eigenvalues tab shows the Eigenvalues, and the PrincipleComponents tab the principle component axes.
<br><br>
<h3>Chemodiversity</h3>
This is a beta version, please use it with that in mind! You can calculate various alpha diversity metrics on your data. Presently, the only measures of beta diversity available are dissimilarity matrices. This therefore provides calculation and visualization via heatmap of multivariate beta diversity metrics, i.e. dissimilarity matrices. We plan to add classical beta diversity measures in the future.
<br><br>
Presently, we use exclusively ecological definitions of alpha and beta diversity. If you have feedback or suggestions please feel free to let us know!
<br><br>
<h3>PERMANOVA</h3>
Calculates a PERMANOVA using the adonis2 function of the vegan package. PERMANOVA is a permutational test; some experimental designs prohibit full permutation and require that the user set which groups can be permuted between one another and which cannot. For a brief explanation of PERMANOVA, see the vegan documentation: <a href=https://rdrr.io/cran/vegan/man/adonis.html>https://rdrr.io/cran/vegan/man/adonis.html</a>. For an explanation of permutational design, see the documentation of the how function in the permute package: <a href=https://www.rdocumentation.org/packages/permute/versions/0.9-7/topics/how>https://www.rdocumentation.org/packages/permute/versions/0.9-7/topics/how</a>. For a more detailed treatment of both topics, see the video tutorial on multivariate statics by Gavin Simpson: <a href=https://youtu.be/PR1B_JkO49s>Advanced community ecological data analysis using vegan</a>. The section on permutational tests runs from 1:36:50 to 2:16:40.

<br><br>
To carry out the PERMANOVA, first build the formula. Choose the number of predictor variables you want to use (maximum is the number of treatment groups in your grouping file) under "Number of predictor variables", and then choose the order and relationships of the variables. The notation follows the standard used for models in R: "+" indicates a simple additive relationship between variables, ":" means an interaction between two variables, and "*" is shorthand for including both (i.e., a*b = a + b + a:b). Use the formula preview to ensure that you have the variables you want.

<br><br>

Because PERMANOVA is a random process, you must set the seed to provide a starting point for the random process (set.seed() in R). The number you choose is completely arbitrary, but make a note of it. If you wish to repeat the analysis, you must use the same seed if you want to obtain the same results. 

<br><br>

You must also choose which dissimilarity matrix you believe to be most suitable for your data.

<br><br>

The collapsible boxes Permutational design and Permutation details provide an interface to the how() family of functions for setting permutational design. If your analysis allows for free permutation, you do not need to use these settings. However, <b>you must at least open and close each box once before clicking Calculate PERMANOVA.</b>

<br><br>

After calculating the PERMANOVA, you can preview the statistical results in the MetIgel window, and download them as an Excel file. In this file, the sheet "Call" will show the formula you used for calculation, the sheet "Design" will show the arguments passed to how() in setting the permutational design as well as which dissimilarity matrix you used, and the sheet "Results" shows the results of the statistical test.

<h3>NMDS</h3>
Use the links on the page to read about NMDS. You can view and save both the stress plot, showing the goodness of fit of your NMDS, and the NMDS ordination itself. You can also choose the dissimilarity matrix, how many dimensions you want -- typically 2, sometimes 3, rarely more -- and the number of random starts the function can use to try to find a stable solution. The vegan documentation typically refers to species and communities/sites. Your features are analogous to species, and your samples are communities/sites.

<br><br>
Note that the calculation of NDMS automatically uses the settings autotransform=FALSE, as is recommended for use with non-community data. If there is interest, this can be converted so the user can choose whether autotransform is used or not. The NMDS engine monoMDS is used, rendering the noshare argument irrelevant (as its step-across methodology only applies to the isoMDS method). wascores, which gives "species scores" (in our case,scores/weights given to the individual features or classification level of your choice), defaults to TRUE but can be changed.

<br><br>

Downloading the results gives an Excel file with two sheets. The sheet "RunInfo" provides the settings you chose for running the NMDS. The sheet "Scores" shows the scores (locations) on the NMDS axes (however many you chose to have) for each sample, as well as the treatment information for each sample. In NMDS parlance, these are the "sites" scores, not the "species" scores (which would be the compounds or whatever chemical class level you've chosen to carry out the NMDS at).

<br><br><br>
We are planning a second release in the indefinite future, so some features may change or be updated then. Feedback is welcome.
<br><br>

<h2>References</h2>

Broeckling CD, Afsar FA, Neumann S, Ben-Hur A, Prenni JE. RAMClust: a novel feature clustering method enables spectral-matching-based annotation for metabolomics data. Anal Chem. 2014 Jul 15;86(14):6812-7. doi: 10.1021/ac501530d.<br><br>
Kim S, Thiessen PA, Bolton EE, Bryant SH. PUG-SOAP and PUG-REST: web services for programmatic access to chemical information in PubChem. Nucleic Acids Res. 2015;43(W1):W605-11.<br><br>
Djoumbou Feunang Y, Eisner R, Knox C, Chepelev L, Hastings J, Owen G, Fahy E, Steinbeck C, Subramanian S, Bolton E, Greiner R, and Wishart DS. ClassyFire: Automated Chemical Classification With A Comprehensive, Computable Taxonomy. Journal of Cheminformatics, 2016, 8:61. DOI: 10.1186/s13321-016-0174-y<br><br>
Petrén H., T.G. Köllner and R.R Junker. 2022. Quantifying chemodiversity considering biochemical and structural properties of compounds with the R package chemodiv. bioRxiv doi: 10.1101/2022.06.08.495236.


</body>
</html>
