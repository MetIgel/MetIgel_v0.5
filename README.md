# MetIgel development branch

The working branch of MetIgel development. (Working = being worked on, not necessarily functioning!) Look here for latest bug fixes and features. Note, however, that the program as downloaded here may not always be functional, as it may be in the middle of testing or bug fixes.
If you would like to use the stable version of MetIgel, go to the [branch for version 1.0.](https://codeberg.org/MetIgel/MetIgel/src/branch/MetIgel_v1.0)

<h2>How to download and install MetIgel</h2>
Note: MetIgel requires that you have RStudio installed on your machine.

<h3>How to download</h3>
1.	Go to: https://codeberg.org/MetIgel/ for the development version, or to https://codeberg.org/MetIgel/MetIgel/src/branch/MetIgel_v1.0 for the stable version
2.	Click the download arrow

![Click the small download button in the top right corner of the file list, next to where the URL of the repo is displayed.](shiny/www/codeberg_download_button.png)

3.	Click “Download ZIP”
4.	Save the ZIP file and extract it to a file location of your choice
5.	Go to the newly extracted folder
6.	Open the .Rproj file to open the R project containing all necessary files in RStudio
7.	In the RStudio file navigator, click the folder “shiny” and then the file “app.R”

![Unless you have customized it otherwise, one of the four RStudio panels is a file navigator which lists the files and folders of the folder you are currently in. Click the folder labeled "shiny".](shiny/www/rstudio_shiny_folder.png)
![You now see the contents of the folder "shiny", including a file called "app.R". Click the "app.R" file to open it in RStudio.](shiny/www/rstudio_app_file.png)

8.	Click the button “Run App” 
![In the top right-hand corner of the window displaying the app.R file, there is a small button with a green triangle that says "Run App". Click this button.](shiny/www/rstudio_run_app.png)

9. You're ready to use MetIgel! Note that the very first time opening the program may take a while, as it must download any R packages that you do not have installed into your R library. 

Please cite as:

Smith, L. C., & Schedl, A. (2023). MetIgel (1.0.0). Zenodo. https://doi.org/10.5281/zenodo.10640157

BibTeX field:
```
@software{smith_2024_10640157,
  author       = {Smith, Linnea C. and
                  Schedl, Andreas},
  title        = {MetIgel},
  month        = feb,
  year         = 2024,
  publisher    = {Zenodo},
  version      = {1.0.0},
  doi          = {10.5281/zenodo.10640157},
  url          = {https://doi.org/10.5281/zenodo.10640157}
}
```